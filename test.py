# -*- coding: utf-8 -*-

from Crypto.Cipher import AES
import base64
import re
class AESCrypt():

    def __init__(self,key):
        self.key = str.encode(key)
        self.iv = bytes(16)
        self.MODE = AES.MODE_CBC
        self.block_size = 16
        self.padding = lambda data: data + (self.block_size - len(data.encode('utf-8')) % self.block_size) * chr(self.block_size - len(data.encode('utf-8')) % self.block_size)
        self.unpadding = lambda data: data[:-ord(data[-1])]

    def balls_decrypted(self, text):
        plain_base64 = base64.b64decode(text)
        cryptor = AES.new(self.key, self.MODE, self.iv)
        decrypt_text = cryptor.decrypt(plain_base64)
        plain_text = self.unpadding(decrypt_text.decode("utf-8"))
        print(plain_text)

if __name__ == '__main__':

    ase = AESCrypt("ZGJfXxZNGPqWAC53")
    v = "KMLeX7zYGc3SBZi55/BR0VnMybZb29CrFJFl3ac8/k="

    str = ase.balls_decrypted(v)

    # sendclass = AESCrypt(v)


    print str