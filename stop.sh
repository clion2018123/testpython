#!/bin/bash

ps -ef|netstat -anp|grep 7700|grep "python"|grep -v grep|awk '{printf $7}'|cut -d '/' -f 1|xargs kill -9
echo "killing apk server, port:7700"

ps -ef|netstat -anp|grep 7701|grep "python"|grep -v grep|awk '{printf $7}'|cut -d '/' -f 1|xargs kill -9
echo "killing file server, port:7701"